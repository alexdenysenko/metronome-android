package com.example.alexandr.metronome;

import android.app.Activity;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Vibrator;

public class AndroidFeaturesManager {

    Vibrator vibr ;
    Camera camera;
    MediaPlayer mediaPlayer;

    public AndroidFeaturesManager(Activity activity) {
        vibr = (Vibrator) activity.getSystemService(Activity.VIBRATOR_SERVICE);
        mediaPlayer = MediaPlayer.create(activity, R.raw.sound_tick);
    }

    public void enable(boolean isVibr, boolean isFlash, boolean isSound) {
        if(isVibr) {
            vibr.vibrate(140);
        }
        if(isFlash) {
            camera = Camera.open();
            Camera.Parameters params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
        }
        if(isSound) {
            mediaPlayer.start();
        }
    }

    public void disable() {
        if (vibr.hasVibrator()) {
            vibr.cancel(); // This event does not happen exactly
        }

        if(camera != null) {
            Camera.Parameters params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.release();
            camera = null;
        }

        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }
}
