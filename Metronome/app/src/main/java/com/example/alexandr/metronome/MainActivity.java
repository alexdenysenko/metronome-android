package com.example.alexandr.metronome;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import com.example.alexandr.metronome.CustomControl.FooterFrameButton;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final String broadcastString = "com.example.alexandr.metronome";

    Button btnToggle;
    FrameLayout upFrame;
    FrameLayout downFrame;
    ImageView ivIndicator;

    FooterFrameButton vibrationFrame;
    ImageView vibrationImage;
    FooterFrameButton flashFrame;
    ImageView flashImage;
    FooterFrameButton soundFrame;
    ImageView soundImage;
    EditText etBpm;
    Boolean isStarted = false; //flag for toggle button

    SeekBar sbFrequency;

    Boolean isConnected = false;
    ServiceConnection sConn;
    Intent intent;
    MetronomeService metronomeService;

    IntentFilter intentFilter;
    AndroidFeaturesManager manager;
    Handler delayHandler;
    boolean onTimeDelay = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        btnToggle = (Button) findViewById(R.id.btnToggle);
        upFrame = (FrameLayout) findViewById(R.id.upFrame);
        downFrame = (FrameLayout) findViewById(R.id.downFrame);

        sbFrequency = (SeekBar) findViewById(R.id.seekBar);
        ivIndicator = (ImageView) findViewById(R.id.ivIndicator);

        vibrationFrame = (FooterFrameButton) findViewById(R.id.vibrationFrame);
        vibrationFrame.setOnClickListener(this);
        flashFrame = (FooterFrameButton) findViewById(R.id.flashFrame);
        flashFrame.setOnClickListener(this);
        soundFrame = (FooterFrameButton) findViewById(R.id.soundFrame);
        soundFrame.setOnClickListener(this);

        vibrationImage = (ImageView) findViewById(R.id.ivVibration);
        vibrationImage.setImageResource(R.drawable.vibration_button_2);

        flashImage = (ImageView) findViewById(R.id.ivFlash);
        flashImage.setImageResource(R.drawable.flash_button_2);
        soundImage = (ImageView) findViewById(R.id.ivSound);
        soundImage.setImageResource(R.drawable.sound_button_2);

        etBpm = ((EditText) findViewById(R.id.etBpm));
        etBpm.setOnKeyListener(new View.OnKeyListener() {
               public boolean onKey(View v, int keyCode, KeyEvent event) {
                   if (event.getAction() == KeyEvent.ACTION_DOWN  &&
                           (keyCode == KeyEvent.KEYCODE_ENTER)) {
                       Integer value = Integer.parseInt(etBpm.getText().toString());
                       setAndUpdate(value);
                       return true;
                   }
                   return false;
               }
           }
        );

        btnToggle.setOnClickListener(this);
        upFrame.setOnClickListener(this);
        downFrame.setOnClickListener(this);

        intent = new Intent(this, MetronomeService.class);

        delayHandler = new Handler();
        sConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d("MyLog", "MainActivity onServiceConnected");
                metronomeService = ((MetronomeService.MyBinder) service).getService();
                isConnected = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d("MyLog", "MainActivity onServiceDisconnected");
                isConnected = false;
            }
        };

        sbFrequency.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                setAndUpdate(seekBar.getProgress() + MetronomeService.MIN_FREQUENCY);
            }
        });

        manager = new AndroidFeaturesManager(this);
        intentFilter = new IntentFilter(broadcastString);
        registerReceiver(br, intentFilter);
        startService(intent);
    }


    //Listen request`s at Service
    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            delayHandler.post(run);
        }
    };

    Runnable run = new Runnable() {
        @Override
        public void run() {
            if (onTimeDelay) {
                ivIndicator.setImageResource(R.drawable.indicator_white);
                onTimeDelay = false;
                manager.disable();
            } else {
                ivIndicator.setImageResource(R.drawable.indicator_green);
                onTimeDelay = true;
                manager.enable(vibrationFrame.getPressed(), flashFrame.getPressed(), soundFrame.getPressed());
                delayHandler.postDelayed(this, 140);
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        bindService(intent, sConn, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isConnected) return;
        delayHandler.removeCallbacks(run);
        unbindService(sConn);
        isConnected = false;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnToggle:
                if(isStarted) {
                    metronomeService.stop();
                    btnToggle.setText(R.string.start);
                    isStarted = false;
                } else {
                    metronomeService.start();
                    btnToggle.setText(R.string.stop);
                    isStarted = true;
                }
                break;
            case R.id.downFrame:
                if (!isConnected) return;
                setAndUpdate(metronomeService.getFrequency() - 21);
                break;
            case R.id.upFrame:
                if (!isConnected) return;
                setAndUpdate(metronomeService.getFrequency() + 21);
                break;
            case R.id.vibrationFrame:
                if(vibrationFrame.isPressed()) {
                    vibrationImage.setImageResource(R.drawable.vibration_button);
                } else {
                    vibrationImage.setImageResource(R.drawable.vibration_button_2);
                }
                Log.d("vibration", "isPressed");
                break;
            case R.id.flashFrame:
                if(flashFrame.isPressed()) {
                    flashImage.setImageResource(R.drawable.flash_button);
                } else {
                    flashImage.setImageResource(R.drawable.flash_button_2);
                }
                Log.d("flash", "isPressed");
                break;

            case R.id.soundFrame:
                if(soundFrame.isPressed()) {
                    soundImage.setImageResource(R.drawable.sound_button);
                } else {
                    soundImage.setImageResource(R.drawable.sound_button_2);
                }
                Log.d("sound", "isPressed");
                break;
        }
    }

    private void setAndUpdate(Integer currentFrequency){
        metronomeService.setFrequency(currentFrequency);
        etBpm.setText(String.format("%s", metronomeService.getFrequency()));
        sbFrequency.setProgress(metronomeService.getFrequency() - MetronomeService.MIN_FREQUENCY);
        if(isStarted) {
            metronomeService.stop();
            metronomeService.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(intent);
    }
}
