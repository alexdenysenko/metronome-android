package com.example.alexandr.metronome.CustomControl;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class FooterFrameButton extends FrameLayout {
    Boolean isPressed = false;
    public FooterFrameButton(Context context) {
        super(context);
    }

    public FooterFrameButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FooterFrameButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean isPressed() {
        if(isPressed) {
            isPressed = false;

        } else {
            isPressed = true;
        }
        return isPressed;
    }

    public boolean getPressed() {
        return isPressed;
    }
}
