package com.example.alexandr.metronome;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import java.util.Timer;
import java.util.TimerTask;

public class MetronomeService extends Service {
    public static final Integer MIN_FREQUENCY = 40;
    public static final Integer MAX_FREQUENCY = 208;

    MyBinder binder = new MyBinder();
    Timer timer;
    TimerTask timerTask;

    private Integer frequency = (MAX_FREQUENCY - MIN_FREQUENCY) / 2 + MIN_FREQUENCY;   //standard

    @Override
    public void onCreate() {
        super.onCreate();
        timer = new Timer();
    }

    void schedule() {
        if(timerTask != null) timerTask.cancel();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Intent intent  = new Intent(MainActivity.broadcastString);
                sendBroadcast(intent);
            }
        };
        timer.schedule(timerTask, 0, convertFrequencyToMilliseconds(getFrequency()));
    }

    Integer convertFrequencyToMilliseconds(Integer frequency) {
        float delay = (60 / (float)frequency * 1000);

        return (int)(delay);
    }

    public void start() {
        if(timer == null) timer = new Timer();
        schedule();
    }

    public void stop() {
        timer.cancel();
        timer = null;
    }


    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {

        this.frequency = frequency;
        dataVerification();
    }

    private void dataVerification() {
        if(frequency >= MAX_FREQUENCY) frequency = MAX_FREQUENCY;
        else if(frequency <= MIN_FREQUENCY) frequency =  MIN_FREQUENCY;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MyLog", "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("MyLog", "onBind");
        return binder;
    }

    @Override
    public void onDestroy() {
        Log.d("MyLog", "onDestroy");
        super.onDestroy();
    }

    class MyBinder extends Binder {
        MetronomeService getService() {
            return MetronomeService.this;
        }
    }
}